﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;

using Warehouse.Entities.Models.ProductModels;

namespace Warehouse.BusinessLogicLayer.Services
{
    public class ProductService : IProductService
    {
        public readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<ProductDto> GetById(int Id)
        {
            var mappedModel = await _productRepository.GetById(Id);
            return Mapper.Map<Product, ProductDto>(mappedModel);
        }

        public async Task<IEnumerable<ProductDto>> GetAll()
        {
            var mappedModel = await _productRepository.GetAll();

            return Mapper.Map<IEnumerable<ProductDto>>(mappedModel);
        }

        public async Task<int> Create(ProductDto createProduct)
        {
            var mappedModel = Mapper.Map<ProductDto, Product>(createProduct);
           return await _productRepository.Create(mappedModel);
        }

        public async Task Update(ProductDto updateProduct)
        {
            var mappedModel = Mapper.Map<ProductDto, Product>(updateProduct);
            await _productRepository.Update(mappedModel);
        }

        public async Task Delete(int Id)
        {
            var deletedProduct = await _productRepository.GetById(Id);
            if (deletedProduct == null)
            {
                throw new Exception("Item Is Null");
            }
            await _productRepository.Delete(deletedProduct);
        }
    }
}
