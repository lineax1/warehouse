﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Entities.Interfaces
{
   public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetById(int Id);
        Task<IEnumerable<TEntity>> GetAll();
        Task Delete(TEntity Id);
        Task Update(TEntity entity);
        Task<int> Create(TEntity entity);
        Task UpdateRange(IEnumerable<TEntity> entities);
        Task CreateRange(IEnumerable<TEntity> entities);
    }
}
