﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.DataAccessLogic.Repository.GenericRepository;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;

namespace Warehouse.DataAccessLogic.Repository.EntityFramework
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public readonly ApplicationContext _context;
        public ProductRepository(ApplicationContext context)
            : base(context)
        {
            _context = context;
        }


    }
}
