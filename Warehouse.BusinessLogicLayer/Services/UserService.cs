﻿using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Warehouse.BusinessLogicLayer.Helpers;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Enums;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models.AccountModels;
using Warehouse.Entities.Models.UserModels;

namespace Warehouse.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly HashHelper _hashHelper;
        private readonly JwtHelper _jwtHelper;
        private readonly IAccountService _accountService;
        private readonly IAccountRepository _accountRepository;

        public UserService(IUserRepository userRepository, HashHelper hashHelper,
            JwtHelper jwtHelper, IAccountRepository accountRepository, IAccountService accountService)
        {
            _userRepository = userRepository;
            _hashHelper = hashHelper;
            _jwtHelper = jwtHelper;
            _accountRepository = accountRepository;
            _accountService = accountService;
        }

        public async Task<object> Login(User userLogin)
        {
            _hashHelper.GetToken(userLogin.Password);
            userLogin.Password = _hashHelper.GetHashString(userLogin.Password);

            var checkUser = await _userRepository.GetByCredantial(userLogin.Password, userLogin.Login);
            if (checkUser == null)
            {
                throw new Exception("Incorrect Data");
            }

            var role = checkUser.Account.Role;
            var accountId = checkUser.Account.Id;
            var stringRole = role.ToString();

            var token = _jwtHelper.GenerateEncodedToken(userLogin.Login);
            return new { role = stringRole, token = token, accountId = accountId };
        }

        public async Task Registration(UserDto userRegistration)
        {
            var mappedModel = Mapper.Map<UserDto, User>(userRegistration);

            var checklogin = await _userRepository.GetByLogin(mappedModel.Login);
            if (checklogin == null)
            {
                throw new Exception("existing user");
            }

            mappedModel.Password = _hashHelper.GetHashString(userRegistration.Password);

            Account account = new Account()
            {
                Email = mappedModel.Login
            };
            mappedModel.AccountId = await _accountRepository.DefaultCreation(account);

            await _userRepository.DefaultCreation(mappedModel);
        }
    }
}