﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.MicroKernel.Lifestyle.Scoped;
using System;
using Castle.MicroKernel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.DataAccessLogic;
using Warehouse.DataAccessLogic.Repository.EntityFramework;
using Warehouse.Entities.Interfaces;
using Warehouse.BusinessLogicLayer.Services;
using Warehouse.BusinessLogicLayer.Helpers;
using Warehouse.Entities.Models;

namespace Warehouse.WindsorDI.ConfigDi
{
    public class Installer : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
            Component.For<ApplicationContext>().DependsOn(Property.ForKey<string>()
            .Eq(@"Data Source=DESKTOP-L45B002; Initial Catalog=Warehouse; Integrated Security=True"))
            );
            container.Register(
                Component.For<IProductRepository>().ImplementedBy<ProductRepository>().LifestylePerWebRequest()
             );
            container.Register(
              Component.For<IOrderRepository>().ImplementedBy<OrderRepository>().LifestylePerWebRequest()
            );
            container.Register(
              Component.For<IUserRepository>().ImplementedBy<UserRepository>().LifestylePerWebRequest()
             );
            container.Register(
               Component.For<IShipmentRepository>().ImplementedBy<ShipmentRepository>().LifestylePerWebRequest()
             );
            container.Register(
                Component.For<IAccountRepository>().ImplementedBy<AccountRepository>().LifestylePerWebRequest()
              );
            container.Register(
               Component.For<IOrderProductRepository>()
               .ImplementedBy<OrderProductRepository>().LifestylePerWebRequest()
             );


            container.Register(
               Component.For<IProductService>().ImplementedBy<ProductService>().LifestylePerWebRequest()
            );
            container.Register(
              Component.For<IOrderService>().ImplementedBy<OrderService>().LifestylePerWebRequest()
            );
            container.Register(
              Component.For<IUserService>().ImplementedBy<UserService>().LifestylePerWebRequest()
             );
            container.Register(
               Component.For<IShipmentService>().ImplementedBy<ShipmentService>().LifestylePerWebRequest()
             );
            container.Register(
                Component.For<IAccountService>().ImplementedBy<AccountService>().LifestylePerWebRequest()
             );
            container.Register(
               Component.For<HashHelper>().ImplementedBy<HashHelper>().LifestylePerWebRequest());
            container.Register(
                Component.For<JwtHelper>().ImplementedBy<JwtHelper>().LifestylePerWebRequest());
            container.Register(
                Component.For<UploadImageHelper>().ImplementedBy<UploadImageHelper>()
                .DependsOn(Dependency.OnValue("cloudinaryConfig", new CloudinaryConfigModel()))
                .LifestylePerWebRequest());

        }
    }
}
