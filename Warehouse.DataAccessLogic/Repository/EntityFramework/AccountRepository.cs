﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.DataAccessLogic.Repository.GenericRepository;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;

namespace Warehouse.DataAccessLogic.Repository.EntityFramework
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        private ApplicationContext _context;
        public AccountRepository(ApplicationContext context)
            : base(context)
        {
            _context = context;
        }
        public async Task<Account> GetByEmail(string email)
        {
            var findedEmail = await _context.Accounts.FindAsync(email);
            return findedEmail;
        }
        public async Task<int> DefaultCreation(Account account)
        {
            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();
            return account.Id;
        }
    }
}
