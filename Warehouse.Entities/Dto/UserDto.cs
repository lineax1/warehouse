﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Enums;
using Warehouse.Entities.Models.AccountModels;

namespace Warehouse.Entities.Models.UserModels
{
    public class UserDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public Provider Provider { get; set; }
    }
}
