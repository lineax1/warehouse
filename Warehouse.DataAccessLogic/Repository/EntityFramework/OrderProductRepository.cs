﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.DataAccessLogic.Repository.GenericRepository;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models.OrderModels;

namespace Warehouse.DataAccessLogic.Repository.EntityFramework
{
    public class OrderProductRepository: IOrderProductRepository
    {
        private ApplicationContext _context;
        public OrderProductRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task Add(IEnumerable<Entities.Entites.OrderProduct> models)
        {
            _context.OrderProducts.AddRange(models);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<OrderProduct>> GetAll()
        {
            return  _context.Set<OrderProduct>().ToList();
        }
    }
}
