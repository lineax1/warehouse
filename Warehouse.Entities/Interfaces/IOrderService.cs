﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Models.OrderModels;
using Warehouse.Entities.Models.UserModels;

namespace Warehouse.Entities.Interfaces
{
   public interface IOrderService
    {
       Task<OrderList> GetAll();
       Task<OrderDto> GetById(int Id);
       Task Create(OrderDto createOrder);
       Task Update(OrderDto updateOrder);
       Task Delete(int id);
    }
}
