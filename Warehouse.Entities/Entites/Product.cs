﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Enums;

namespace Warehouse.Entities.Entites
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string SKU { get; set; }
        public decimal Price { get; set; }
        public TypeProduct TypeProduct { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }

        public Product()
        {
        }
    }
}
    