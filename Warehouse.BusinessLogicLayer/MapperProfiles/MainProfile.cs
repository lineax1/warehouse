﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Enums;
using Warehouse.Entities.Models.AccountModels;
using Warehouse.Entities.Models.OrderModels;
using Warehouse.Entities.Models.ProductModels;
using Warehouse.Entities.Models.ShipmentModels;
using Warehouse.Entities.Models.UserModels;

namespace Warehouse.BusinessLogicLayer.MapperProfiles
{
    public class MainProfile : Profile
    {
        public MainProfile()
        {
            CreateMap<Product, ProductDto>()
                .ForMember(x => x.TypeProduct, y => y.MapFrom(z => z.TypeProduct.ToString("g")));

            CreateMap<ProductDto, Product>();

            CreateMap<Shipment, ShipmentDto>()
                .ForMember(x => x.ProductList, y => y.MapFrom(z => z.ProductList));
            CreateMap<ShipmentDto, Shipment>();

            CreateMap<OrderDto, Order>();
            CreateMap<Order, OrderDto>();
                // .ForMember(x => x., y => y.MapFrom(z => z.ProductList));
                //.ForMember(x => x.ProductList, y => y.MapFrom(z => z.ProductList));


            CreateMap<AccountDto, Account>();
            CreateMap<Account, AccountDto>()
                .ForMember(x => x.UserList, y => y.MapFrom(z => z.UserList));

            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>()
                .ForMember(x => x.Password, y => y.Ignore());
        }
    }
}
