﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Models.OrderModels;

namespace Warehouse.Entities.Interfaces
{
    public interface IOrderProductRepository
    {
        Task Add(IEnumerable<OrderProduct> models);
        Task<IEnumerable<OrderProduct>> GetAll();
    }
}
