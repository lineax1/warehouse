﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models.OrderModels;

namespace Warehouse.API.Controllers
{
    public class OrderController : ApiController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        //order/getbyid
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int Id)
        {
            var findedOrder = await _orderService.GetById(Id);
            return Ok(findedOrder);
        }
        //order/getall
        [HttpGet]
        public async Task<IHttpActionResult> GetAll()
        {
           var ordersList = await _orderService.GetAll();
           return Ok(ordersList);
        }
        //order/create
        [HttpPost]
        public async Task<IHttpActionResult> Create(OrderDto createOrder)
        {
            await _orderService.Create(createOrder);
            return Ok();
        }
        //order/update
        [HttpPut]
        public async Task<IHttpActionResult> Update(OrderDto updateOrder)
        {
            await _orderService.Update(updateOrder);
            return Ok();
        }
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int Id)
        {
            await _orderService.Delete(Id);
            return Ok();
        }
    }
}
