﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.DataAccessLogic.Repository.GenericRepository;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;


namespace Warehouse.DataAccessLogic.Repository.EntityFramework
{
    public class ShipmentRepository : GenericRepository<Shipment>, IShipmentRepository
    {
        public readonly ApplicationContext _context;
        public ShipmentRepository(ApplicationContext context)
            : base(context)
        {
            _context = context;
        }

    }
}
