﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Entities.Enums
{
    public enum TypeProduct
    {
        Phone,
        Book,
        Laptop,
        Jeans,
    }
}
