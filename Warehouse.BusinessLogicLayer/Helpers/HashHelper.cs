﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.BusinessLogicLayer.Services
{
    public class HashHelper
    {
        public byte[] GetToken(string Password)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
            {
                var tokens = algorithm.ComputeHash(Encoding.UTF8.GetBytes(Password));

                return tokens;
            }
        }

        public string GetHashString(string Password)
        {
            StringBuilder stringBuiler = new StringBuilder();
            foreach (byte b in GetToken(Password))
                stringBuiler.Append(b.ToString("X2"));

            var token = stringBuiler.ToString();
            return token;
        }
    }
}
