﻿using AutoMapper;
using Castle.Windsor;
using Castle.Windsor.Installer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Routing;
using Warehouse.API.Castle.DI;
using Warehouse.BusinessLogicLayer.MapperProfiles;
using Warehouse.WindsorDI.ConfigDi;

namespace Warehouse.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            var container = new WindsorContainer();
            container.Install(FromAssembly.This());
            container.Install(new ControllerInstaller());
            container.Install(new Installer());

            GlobalConfiguration.Configuration.Services.Replace(
            typeof(IHttpControllerActivator),
            new WindsorWebApiControllerActivator(container));

            Mapper.Initialize(x =>
            {
                x.AddProfile<MainProfile>();
            });

            //GlobalConfiguration.Configuration.EnsureInitialized();
            //Mapper.Configuration.AssertConfigurationIsValid(); 

            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings
            //.Add( new Newtonsoft)??
        }
    }
}
