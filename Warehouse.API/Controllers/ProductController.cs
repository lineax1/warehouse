﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models.ProductModels;

namespace Warehouse.API.Controllers
{
    public class ProductController : ApiController
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetAll()
        {
            var getAll = await _productService.GetAll();
            return Ok(getAll);
        }
        //product/getbyid
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int Id)
        {
            var getById = await _productService.GetById(Id);
            return Ok(getById);
        }
        //product/create
        [HttpPost]
        public async Task<IHttpActionResult> Create(ProductDto createProduct)
        {
            await _productService.Create(createProduct);
            return Ok();
        }
        //product//create
        [HttpPut]
        public async Task<IHttpActionResult> Update(ProductDto updateProduct)
        {
            await _productService.Update(updateProduct);
            return Ok();
        }
        //product//delete
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int Id)
        {
            await _productService.Delete(Id);
            return Ok(Id);
        }
    }
}
