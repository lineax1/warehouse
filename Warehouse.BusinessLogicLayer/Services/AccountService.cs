﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models.AccountModels;

namespace Warehouse.BusinessLogicLayer.Services
{
    public class AccountService : IAccountService
    {
        public readonly IAccountRepository _accountRepository;

        public AccountService(IAccountRepository accountRepostiroy)
        {
            _accountRepository = accountRepostiroy;
        }

        public async Task<AccountDto> GetById(int Id)
        {
            var account = await _accountRepository.GetById(Id);
            return Mapper.Map<Account, AccountDto>(account);
        }

        public async Task<AccountDto> GetByEmail(string email)
        {
            var account = await _accountRepository.GetByEmail(email);
            var mappedModel = Mapper.Map<Account, AccountDto>(account);
            return mappedModel;

        }

        public async Task<int> Create(AccountDto createAccount)
        {
            var mappedModel = Mapper.Map<AccountDto, Account>(createAccount);
           return await _accountRepository.Create(mappedModel);
        }

        public async Task Update(AccountDto updateAccount)
        {
            var getAccount = await _accountRepository.GetById(updateAccount.Id);
            var mappedModel = Mapper.Map<AccountDto, Account>(updateAccount);
            mappedModel.Role = getAccount.Role;
            await _accountRepository.Update(mappedModel);
        }

        public async Task Delete(int Id)
        {
            var deletedProduct = await _accountRepository.GetById(Id);
            if (deletedProduct == null)
            {
                throw new Exception("Item Is Null");
            }
            await _accountRepository.Delete(deletedProduct);
        }
    }
}
