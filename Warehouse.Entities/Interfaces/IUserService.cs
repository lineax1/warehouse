﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Models.ProductModels;
using Warehouse.Entities.Models.UserModels;

namespace Warehouse.Entities.Interfaces
{
   public interface IUserService
    {
       Task Registration(UserDto userRegistration);
       Task<object> Login(User userLogin);
    }
}
