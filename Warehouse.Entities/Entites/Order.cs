﻿ using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Entities.Entites
{
    public class Order : BaseEntity
    {
        public decimal TotalSumm { get; set; }

        public string Address { get; set; }
        public virtual Account Account { get; set; } 
        public virtual ICollection<Shipment> ShipmentsList { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
        
        public Order()
        {
        }
    }
}
