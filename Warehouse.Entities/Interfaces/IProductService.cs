﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Models.ProductModels;

namespace Warehouse.Entities.Interfaces
{
    public interface IProductService
    {
        Task<ProductDto> GetById(int Id);
        Task<IEnumerable<ProductDto>> GetAll();
        Task<int> Create(ProductDto createProduct);
        Task Update(ProductDto updateProduct);
        Task Delete(int id);
    }
}
