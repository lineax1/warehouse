﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Enums;

namespace Warehouse.Entities.Models.OrderModels
{
    public class OrderList
    {
        public List<OrderDto> Orders { get; set; }
        public OrderList()
        {
            Orders = new List<OrderDto>();
        }
    }
    public class OrderDto
    {
        public int Id { get; set; }
        public Delivery Delivery { get; set; }
        public string Address { get; set; }
        public IEnumerable<OrderProduct> OrderProducts { get; set; }
        public List<ProductList> ProductList { get; set; }
    }

    public class ProductList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SKU { get; set; }
        public decimal Price { get; set; }
        public string TypeProduct { get; set; }
        public int Quantity { get; set; }
    }
}
