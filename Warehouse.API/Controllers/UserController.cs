﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models.UserModels;

namespace Warehouse.API.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        //user/login
        [HttpPost]
        public async Task<IHttpActionResult> Login(User loginUser)
        {
           var token = await _userService.Login(loginUser);
           return Ok(token);
        }
        //user/registration
        [HttpPost]
        public async Task<IHttpActionResult> Registration(UserDto registrationUser)
        {
           await _userService.Registration(registrationUser);
          return Ok();
        }
    }
}
