﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Enums;

namespace Warehouse.Entities.Entites
{
    public class Account : BaseEntity
    {
        public decimal Cash { get; set; }
        public Role Role { get; set; }
        public string FirstName {get;set;}
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }

        public ICollection<User> UserList { get; set; }
        public ICollection<Order> OrderList { get; set; }


        public Account()
        {
            Role = Role.user;
            OrderList = new List<Order>();
            UserList = new List<User>();
        }
    }
}
