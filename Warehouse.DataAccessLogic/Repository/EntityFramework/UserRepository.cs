﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.DataAccessLogic.Repository.GenericRepository;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Enums;
using Warehouse.Entities.Interfaces;

namespace Warehouse.DataAccessLogic.Repository.EntityFramework
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private ApplicationContext _context;
        public UserRepository(ApplicationContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task<string> GetByLogin(string login)
        {
             _context.Users.FirstOrDefault(x => x.Login == login);
            return login;
        }

        public async Task<User> GetByCredantial(string password, string login)
        {
            var userResult = _context.Users.Include("Account").FirstOrDefault(x => x.Password == password && x.Login == login);
            return (userResult);
        }

        public async Task<int> DefaultCreation(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user.Id;
        }
    }
}
