﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models;
using Warehouse.Entities.Models.ProductModels;
using Warehouse.Entities.Models.ShipmentModels;

namespace Warehouse.BusinessLogicLayer.Services
{
    public class ShipmentService : IShipmentService
    {
        public readonly IShipmentRepository _shipmentRepository;
        public readonly IProductRepository _productRepository;

        public ShipmentService(IShipmentRepository shipmentRepository)
        {
            _shipmentRepository = shipmentRepository;
        }

        public async Task<ShipmentDto> GetById(int Id)
        {
            var mappedModel = await _shipmentRepository.GetById(Id);
            return Mapper.Map<Shipment, ShipmentDto>(mappedModel);
        }

        public async Task<IEnumerable<ShipmentDto>> GetAll()
        {
            var mappedModel = await _shipmentRepository.GetAll();
            return Mapper.Map<IEnumerable<Shipment>, IEnumerable<ShipmentDto>>(mappedModel);
        }

        public async Task Update(ShipmentDto updateShipment)
        {
            var mappedModel = Mapper.Map<ShipmentDto, Shipment>(updateShipment);
            await _shipmentRepository.Update(mappedModel);
        }

        public async Task<int> Create(ShipmentDto createShipment)
        {
            var mappedModel = Mapper.Map<ShipmentDto, Shipment>(createShipment);
           return await _shipmentRepository.Create(mappedModel);
        }

        public async Task Delete(int Id)
        {
            var deletedShipment = await _shipmentRepository.GetById(Id);
            if (deletedShipment == null)
            {
                throw new Exception("Item is Null");
            }
            await _shipmentRepository.Delete(deletedShipment);
        }
    }
}

