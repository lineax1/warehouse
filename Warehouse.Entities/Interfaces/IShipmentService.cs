﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Models.ShipmentModels;

namespace Warehouse.Entities.Interfaces
{
   public interface IShipmentService
    {
       Task<ShipmentDto> GetById(int Id);
       Task<IEnumerable<ShipmentDto>> GetAll();
       Task<int> Create(ShipmentDto createShipment);
       Task Update(ShipmentDto updateShipment);
       Task Delete(int Id);
    }
}
