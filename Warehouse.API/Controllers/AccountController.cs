﻿using CloudinaryDotNet.Actions;
using Newtonsoft.JsonResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Web;
using System.Web.Http;
using Warehouse.BusinessLogicLayer.Helpers;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models;
using Warehouse.Entities.Models.AccountModels;
using CloudinaryDotNet;
using System.Web.Hosting;
using System.IO;

namespace Warehouse.API.Controllers
{
    public class AccountController : ApiController
    {
        private readonly IAccountService _accountService;
        private readonly UploadImageHelper _uploadHelper;

        public AccountController(IAccountService accountService, UploadImageHelper uploadHelper)
        {
            _accountService = accountService;
            _uploadHelper = uploadHelper;
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var findedAccount = await _accountService.GetById(id);
            return Ok(findedAccount);
        }
        [HttpPost]
        public async Task<IHttpActionResult> GetByEmail(string email)
        {
            var findedAccount = await _accountService.GetByEmail(email);
            return Ok(findedAccount);
        }
        [HttpPost]
        public async Task<IHttpActionResult> Create(AccountDto createAccount)
        {
            await _accountService.Create(createAccount);
            return Ok();
        }
        [HttpPut]
        public async Task<IHttpActionResult> Update(AccountDto updateAccount)
        {
            await _accountService.Update(updateAccount);
            return Ok();
        }
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int Id)
        {
            await _accountService.Delete(Id);
            return Ok();
        }

        [HttpPost]
        public string Upload()
        {
            var httpRequest = HttpContext.Current.Request;
            JsonResult trem = new JsonResult();
            ImageUploadResult uploadResult = new ImageUploadResult();
            foreach (string file in httpRequest.Files)
            {
                var upload = httpRequest.Files[file];
                if (upload != null)
                {
                    Account account = new Account(
                        "dybnlnrij",
                        "218932683361545",
                        "o4bKYXhXcpcJKMYS0D0je2BX1gc");

                    CloudinaryDotNet.Cloudinary cloudinary = new CloudinaryDotNet.Cloudinary(account);
                    string fileName = System.IO.Path.GetFileName(upload.FileName);
                    upload.SaveAs(HostingEnvironment.MapPath("~/" + fileName));
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(HostingEnvironment.MapPath("~/" + fileName)),
                        PublicId = User.Identity.Name + fileName,
                        Tags = "special, for_homepage"
                    };

                    uploadResult = cloudinary.Upload(uploadParams);
                    File.Delete(HostingEnvironment.MapPath("~/" + fileName));
                }
            }
            return uploadResult.Uri.ToString();
        }
    }
}

