﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models.ShipmentModels;

namespace Warehouse.API.Controllers
{
    public class ShipmentController : ApiController
    {
        private readonly IShipmentService _shipmentService;

        public ShipmentController(IShipmentService shipmentService)
        {
            _shipmentService = shipmentService;
        }
        
        //shipment/getbyid
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int Id)
        {
            var getById = await _shipmentService.GetById(Id);
            return Ok(getById);
        }
        //shipment/getall
        [HttpGet]
        public async Task<IHttpActionResult> GetAll()
        {
            var getAll = await _shipmentService.GetAll();
            return Ok(getAll);
        }
        //shipment/create
        [HttpPost]
        public async Task<IHttpActionResult> Create(ShipmentDto shipment)
        {
            await _shipmentService.Create(shipment);
            return Ok();
        }
        //shipment/update
        [HttpPut]
        public async Task<IHttpActionResult> Update(ShipmentDto shipment)
        {
            await _shipmentService.Update(shipment);
            return Ok();
        }
        //shipment/delete
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int Id)
        {
            await _shipmentService.Delete(Id);
            return Ok();
        }
    }
}
