﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;
using Warehouse.Entities.Models.OrderModels;
using Warehouse.Entities.Models.ProductModels;
using Warehouse.Entities.Models.ShipmentModels;

namespace Warehouse.BusinessLogicLayer.Services
{
    public class OrderService : IOrderService
    {
        public readonly IOrderRepository _orderRepository;
        public readonly IOrderProductRepository _orderProductRepository;
        public readonly IShipmentRepository _shipmentRepository;
        public readonly IProductRepository _productRepository;


        public OrderService(
            IOrderRepository orderRepository,
            IOrderProductRepository orderProductRepository,
            IShipmentRepository shipmentRepository,
            IProductRepository productRepository
            )
        {
            _orderRepository = orderRepository;
            _orderProductRepository = orderProductRepository;
            _shipmentRepository = shipmentRepository;
            _productRepository = productRepository;
        }

        //public async Task<IEnumerable<OrderDto>> GetAll()
        //{
        //    var mappedModel = await _orderRepository.GetAllWithProducts();
        //    return Mapper.Map<IEnumerable<OrderDto>>(mappedModel);
        //}

        public async Task<OrderList> GetAll()
        {
            var mappedModel = await _orderRepository.GetAll();
            var getProduct = await _productRepository.GetAll();
            var getOrderProduct = await _orderProductRepository.GetAll();
           var OrderList =  mappedModel.Select(order => new OrderDto() {
                   Id = order.Id,
                   Address = order.Address,
                   ProductList = MappRange(getProduct.Where(prod => prod.OrderProducts.Where(a => a.OrderId == order.Id).Select(c => c.ProductId).Contains(prod.Id))),
            }).ToList();
           var result = new OrderList() { Orders = OrderList };
           return result;
        }


        public async Task<OrderDto> GetById(int Id)
        {
            var mappedModel = await _orderRepository.GetById(Id);
            return Mapper.Map<Order, OrderDto>(mappedModel);
        }

        public async Task Create(OrderDto createOrder)
        {
            var mappedModel = Mapper.Map<Order>(createOrder);
            var orderEntityId = await _orderRepository.Create(mappedModel);
            var orderProducts = createOrder.OrderProducts.Select(x => new OrderProduct()
            {
                OrderId = orderEntityId,
                ProductId = x.ProductId,
                Quantity = x.Quantity
            });

            await _orderProductRepository.Add(orderProducts);
            await _shipmentRepository.Create(new Shipment()
            {
                Delivery = createOrder.Delivery,
                OredeId = orderEntityId
            });
        }

        public async Task Update(OrderDto updateOrder)
        {
            var mappedModel = Mapper.Map<OrderDto, Order>(updateOrder);
            await _orderRepository.Update(mappedModel);
        }

        public async Task Delete(int Id)
        {
            var deletedOrder = await _orderRepository.GetById(Id);
            if (deletedOrder == null)
            {
                throw new Exception("Item Is Null");
            }
            await _orderRepository.Delete(deletedOrder);
        }
        public List<ProductList> MappRange(IEnumerable<Product> product)
        {
            return product.Select(x => new ProductList()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Price = x.Price,
                    SKU =x.SKU,
                    TypeProduct = x.TypeProduct.ToString(),
                }
                ).ToList();
        }
    }
}
