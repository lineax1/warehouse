﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Enums;

namespace Warehouse.Entities.Entites
{
    public class User : BaseEntity
    {   
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
        public Provider Provider { get; set; }

        public int AccountId { get; set; }
        public Account Account { get; set; }

    }
}
