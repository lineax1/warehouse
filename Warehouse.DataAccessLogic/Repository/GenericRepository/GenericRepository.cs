﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;
using System.Data.Entity.Migrations;

namespace Warehouse.DataAccessLogic.Repository.GenericRepository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public async Task<TEntity> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return _dbSet.ToList();
        }

        public async Task<int> Create(TEntity entity)
        {
            var result = _dbSet.Add(entity);
            await _context.SaveChangesAsync();
            return result.Id;
        }

        public async Task Update(TEntity entity)
        {

            var res = _dbSet.Find(entity.Id);
            if (entity == null)
            {
                return;
            }
            _dbSet.AddOrUpdate(entity);
            await _context.SaveChangesAsync();
        }

        public async Task CreateRange(IEnumerable<TEntity> entites)
        {
            _dbSet.AddRange(entites);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateRange(IEnumerable<TEntity> entities)
        {
            _context.Entry(entities).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task Delete(TEntity entity)
        {
            TEntity item = _dbSet.FirstOrDefault(x => x.Id == entity.Id);
            if (item != null)
            {
                _dbSet.Attach(item);
                _dbSet.Remove(item);
            }
            await _context.SaveChangesAsync();
        }
    }
}
