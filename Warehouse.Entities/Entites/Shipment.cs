﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Enums;

namespace Warehouse.Entities.Entites
{
    public class Shipment : BaseEntity
    {
        public string StatusDescription { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public Delivery Delivery { get; set; }
        public int OredeId { get; set; }
        [NotMapped]
        public virtual Order Order { get; set; }
        public virtual ICollection<Product> ProductList { get; set; }

        public Shipment()
        {
            ProductList = new List<Product>();
        }
    }
}
