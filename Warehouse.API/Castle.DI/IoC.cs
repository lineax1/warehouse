﻿//using Castle.Windsor;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Http.Dependencies;

//namespace Warehouse.API.Castle.DI
//{
//    public class IoC : IDependencyScope
//    {
//        private readonly IWindsorContainer _container;
//        private readonly IDisposable _scope;

//        public IoC(IWindsorContainer container)
//        {
//            this._container = container;
//            this._scope = container;
//        }

//        public object GetService(Type serviceType)
//        {
//            if (_container.Kernel.HasComponent(serviceType))
//            {
//                return _container.Resolve(serviceType);
//            }
//            else
//            {
//                return null;
//            }
//        }

//        public IEnumerable<object> GetServices(Type serviceType)
//        {
//            return this._container.ResolveAll(serviceType).Cast<object>();
//        }

//        public void Dispose()
//        {
//            this._scope.Dispose();
//        }
//    }
//}