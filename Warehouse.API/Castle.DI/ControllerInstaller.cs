﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;

namespace Warehouse.API.Castle.DI
{
    public class ControllerInstaller: IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //container.Register(
            //    Classes.
            //        FromThisAssembly().
            //        If(c => c.Name.EndsWith("Controller")).
            //        LifestyleTransient());

            container.Register(
                Classes.
                    FromThisAssembly().
                    BasedOn<IHttpController>(). 
                    If(c => c.Name.EndsWith("Controller")).
                    LifestyleTransient());
        }
    }
}