﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Enums;
using Warehouse.Entities.Models.ProductModels;

namespace Warehouse.Entities.Models.ShipmentModels
{
    public class ShipmentDto
    {
        public int Id { get; set; }
        public string StatusDescription { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public Delivery Delivery { get; set; }

        public IEnumerable<ProductDto> ProductList { get; set; }
    }
}
