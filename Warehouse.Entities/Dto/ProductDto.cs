﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Enums;
using Warehouse.Entities.Models.ShipmentModels;

namespace Warehouse.Entities.Models.ProductModels
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SKU { get; set; }
        public decimal Price { get; set; }
        public string TypeProduct { get; set; }
        public int Quantity { get; set; }

        public Shipment Shipment { get; set; }
    }
}
