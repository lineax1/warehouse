﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.DataAccessLogic.Repository.GenericRepository;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Interfaces;

namespace Warehouse.DataAccessLogic.Repository.EntityFramework
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        private ApplicationContext _context;
        public OrderRepository(ApplicationContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Order>> GetAllWithProducts()
        {
            return _context.Orders.Include("OrderProducts.Order").Include("Product").ToList();
        }
        public async Task<IEnumerable<Order>> GetByUserId(int id)
        {
            return  _context.Orders.Where(order => order.Account.Id == id);
        }
    }
}
