﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;

namespace Warehouse.Entities.Interfaces
{
   public interface IUserRepository:IGenericRepository<User>
    {
       Task<string> GetByLogin(string login);
       Task<User> GetByCredantial(string password, string login);
       Task<int> DefaultCreation(User user);
    }
}
