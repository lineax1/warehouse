﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Enums;
using Warehouse.Entities.Models.UserModels;

namespace Warehouse.Entities.Models.AccountModels
{
    public class AccountDto
    {
        public int Id { get; set; }
        public decimal Cash { get; set; }
        public Role Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Quantity { get; set; }
        public string Picture { get; set; }

        public IEnumerable<UserDto> UserList { get; set; }
    }
}
