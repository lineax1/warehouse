﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;

namespace Warehouse.Entities.Interfaces
{
   public interface IOrderRepository: IGenericRepository<Order>
    {
       Task<IEnumerable<Order>> GetAllWithProducts();
    }
}
