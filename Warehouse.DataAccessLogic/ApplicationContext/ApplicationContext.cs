﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;

namespace Warehouse.DataAccessLogic
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }


        public ApplicationContext()
            : base(@"Data Source=DESKTOP-L45B002; Initial Catalog=Warehouse; Integrated Security=True")
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderProduct>().ToTable("OrderProducts")
                .HasKey(x=> new {x.ProductId, x.OrderId});

            modelBuilder.Entity<Order>()
                .HasKey(x=>x.Id)
                .HasMany(x => x.OrderProducts)
                .WithRequired(x => x.Order);

            modelBuilder.Entity<Product>()
                .HasKey(x=>x.Id)
                .HasMany(x => x.OrderProducts)
                .WithRequired(x => x.Product);
        }
    }
}
