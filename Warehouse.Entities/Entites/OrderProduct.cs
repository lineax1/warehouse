﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Entities.Entites
{
   public class OrderProduct
    {
       public int OrderId { get; set; }
       public int ProductId { get; set; }
       public int Quantity { get; set; }

       public virtual Product Product { get; set; }
       public virtual Order Order { get; set; }
    }
}
