﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Entites;
using Warehouse.Entities.Models.AccountModels;

namespace Warehouse.Entities.Interfaces
{
   public interface IAccountService
    {
       Task<AccountDto> GetById(int Id);
       Task<AccountDto> GetByEmail(string email);
       Task<int> Create(AccountDto createAccount);
       Task Update(AccountDto updateAccount);
       Task Delete(int Id);
    }
}
